const sql = require("../db.js");
var log = require("log4js").getLogger("regist_v2hbox");

module.exports = {
  register: (req, res) => {
    let mac_lan_regist = req.body.mac_lan;
    let mac_wifi_regist = req.body.mac_wifi;
    let serial_regist = req.body.serial;
    let release_regist = req.body.release;
    let incremental_regist = req.body.incremental;
    let sdk_int_regist = req.body.sdk_int;
    let board_regist = req.body.board;
    let bootloader_regist = req.body.bootloader;
    let brand_regist = req.body.brand;
    let cpu_abi_regist = req.body.cpu_abi;
    let cpu_abi2_regist = req.body.cpu_abi2;
    let device_regist = req.body.device;
    let display_regist = req.body.display;
    let fingerprint_regist = req.body.fingerprint;
    let hardware_regist = req.body.hardware;
    let host_regist = req.body.host;
    let id_regist = req.body.id;
    let manufacturer_regist = req.body.manufacturer;
    let model_regist = req.body.model;
    let product_regist = req.body.product;
    let tags_regist = req.body.tags;
    let time_regist = req.body.time;
    let type_regist = req.body.type;
    let unknown_regist = req.body.unknown;
    let user_regist = req.body.user;
    let ipaddress_regist = req.body.ipaddress;
    let version_app_regist = req.body.version_app;
    let keycode_regist = req.body.keycode;
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, "0");
    var mm = String(today.getMonth() + 1).padStart(2, "0");
    var yyyy = today.getFullYear() + 2;
    var times = today.toTimeString();
    var expirydate = yyyy + "/" + mm + "/" + dd + " " + times.split(" ")[0];
    var slice_maclan_befor = mac_lan_regist.slice(0, 2);
    var slice_maclan_after = mac_lan_regist.slice(-2);
    var usergen = slice_maclan_befor + gencode(4) + slice_maclan_after;

    function gencode(length) {
      var result = "";
      var characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
      var charactersLength = characters.length;
      for (var i = 0; i < length; i++) {
        result += characters.charAt(
          Math.floor(Math.random() * charactersLength)
        );
      }
      return result;
    }
    function chech_keycode(key) {
      var kcode = false;
      let check_keycode_active =
        "SELECT * FROM `keycode_v2hbox` WHERE keycode1 = '" + key + "'";
      sql.query(check_keycode_active, (err, result) => {
        if (err) {
          console.log("sql err ");
          return res.status(500).send(err);
        } else {
          if (result.length > 0) {
            kcode = true;
            console.log(result);
          } else {
            kcode = false;
          }
        }
      });
      return kcode;
    }

    if (mac_lan_regist == " " && serial_regist == " " && keycode_regist == "") {
      //ไม่มีข้อมูล เข้ามา
      Console.log("ไม่มีข้อมูล เข้ามา ");
    } else {
      //มีข้อมูลมา
      let check_kcodes =
        "SELECT * FROM `keycode_v2hbox` WHERE keycode1 = '" +
        keycode_regist +
        "'";
      sql.query(check_kcodes, (err, result) => {
        if (err) {
          console.log("sql err ");
          return res.status(500).send(err);
        } else {
          if (result.length > 0) {
            let check_kcodes_v2hdatabox =
              "SELECT keycode1 FROM v2hbox_data WHERE keycode1 = '" +
              keycode_regist +
              "'";
            sql.query(check_kcodes_v2hdatabox, (err, result) => {
              if (err) {
                console.log("sql err ");
                return res.status(500).send(err);
              } else {
                if (result.length > 0) {
                  res.json({ status: "0", massess: "keycode ถูกใช้งานไปแล้ว" });
                } else {
                  let check_keycode_active = "SELECT v2hbox_data.id as databox_id ,keycode_v2hbox.id as keycode_id, v2hbox_data.keycode1   FROM v2hbox_data  INNER JOIN keycode_v2hbox ON   v2hbox_data.keycode1 = keycode_v2hbox.keycode1 INNER JOIN user_v2hbox ON user_v2hbox.v2hbox_data_id = v2hbox_data.id  WHERE  v2hbox_data.keycode1 = '" + keycode_regist + "'"
                  sql.query(check_keycode_active, (err, result) => {
                      if (err) {
                          console.log("sql err ");
                          return res.status(500).send(err);
                      } else {
                          if (result.length > 0) {
                              //มี key ซ่ำ
                              console.log("nooooooooon");
                              res.json({status:"0", massess: "keycode ถูกใช้งานไปแล้ว" });
                          } else {
                              //check key หมดอายุ start ?
                              let checkkey_start_expiry = "SELECT * FROM `keycode_v2hbox` WHERE keycode1 ='" + keycode_regist + "' AND now() > start_expiry"
                              sql.query(checkkey_start_expiry, (err, result) => {
                                  if (err) {
                                      return res.status(500).send(err);
                                  } else {
                                      if (result.length > 0) {
                                          res.json({status:"0", massess: "Keycode หมดอายุเเล้ว กรุณาติดต่อ 1264" });
                                      } else {
                                          //ไม่มี ซ่ำ insert ได้
                                          //เช็คว่ามี mac / serial หรือยัง ?
                                          let chechbefor_insetboxdt = "SELECT user_v2hbox.usergen , user_v2hbox.v2hbox_data_id  FROM user_v2hbox INNER JOIN v2hbox_data ON v2hbox_data.id = user_v2hbox.v2hbox_data_id WHERE v2hbox_data.mac_lan = '" + mac_lan_regist + "' AND v2hbox_data.mac_wifi ='" + mac_wifi_regist + "' AND v2hbox_data.serial = '" + serial_regist + "'"
                                          sql.query(chechbefor_insetboxdt, (err, result) => {
                                              if (err) {
                                                  return res.status(500).send(err);
                                              } else {
                                                  if (result.length > 0) {
                                                      //res.json({status:"0", massess: "v2hbox ผ่านการลงทะเบียนเเล้ว" });
                                                          let v2hbox_data_ids = result[0].v2hbox_data_id;
                                                          let get_usergen = result[0].usergen;
                                                          let check_usergens = "SELECT * FROM `user_v2hbox` WHERE usergen = '" + result[0].usergen + "' AND NOW() < expiry"
                                                          sql.query(check_usergens, (err, result) => {
                                                              if (err) {
                                                                  return res.status(500).send(err);
                                                              } else {
                                                                  if (result.length > 0) {
                                                                      //มีuserเเล้ว true
                                                                      res.json({ status: "1", massess: "ทำรายการสำเร็จ ผ่านการลงทะเบียนแล้ว",usergen:result[0].usergen });
                                                                  } else {
                                                                      let Update_keycode = "UPDATE v2hbox_data SET keycode1 = '"+keycode_regist+"' WHERE id = '"+v2hbox_data_ids+"'"
                                                                      sql.query(Update_keycode, (err, result) => {
                                                                          if (err) {
                                                                              return res.status(500).send(err);
                                                                          } else {
                                                                              //Update true
                                                                              let Check_boxdata_keycode = "SELECT v2hbox_data.id as databox_id ,keycode_v2hbox.id as keycode_id, v2hbox_data.keycode1   FROM v2hbox_data  INNER JOIN keycode_v2hbox ON  v2hbox_data.keycode1 = keycode_v2hbox.keycode1 WHERE  v2hbox_data.keycode1 = '" + keycode_regist + "'"
                                                                              sql.query(Check_boxdata_keycode, (err, result) => {
                                                                                  if (err) {
                                                                                      return res.status(500).send(err);
                                                                                  } else {
                                                                                      if (result.length > 0) {
                                                                                          let Update_user_v2hbox = "UPDATE user_v2hbox SET keycode_v2hbox_id = '"+result[0].keycode_id+"' WHERE usergen = '"+get_usergen+"'"
                                                                                          sql.query(Update_user_v2hbox, (err, result) => {
                                                                                              if (err) {
                                                                                                  return res.status(500).send(err);
                                                                                              } else {
                                                                                                  let check_boxdata_keycode = "SELECT*FROM user_v2hbox  INNER JOIN keycode_v2hbox ON  user_v2hbox.keycode_v2hbox_id = keycode_v2hbox.id  WHERE  keycode_v2hbox.keycode1 = '" + keycode_regist + "'"
                                                                                                  sql.query(check_boxdata_keycode, (err, result) => {
                                                                                                      if (err) {
                                                                                                          return res.status(500).send(err);
                                                                                                      } else {
                                                                                                          if (result.length > 0) {
                                                                                                              let insert_usergens = "INSERT INTO `user_v2hbox_history`(user_v2hbox_id,keycode_id,activekey)"
                                                                                                                  + "VALUES ('" + result[0].v2hbox_data_id + "','" + result[0].keycode_v2hbox_id + "',NOW());";
                                                                                                              sql.query(insert_usergens, (err, result) => {
                                                                                                                  if (err) {
                                                                                                                      return res.status(500).send(err);
                                                                                                                  } else {
                                                                                                                      res.json({ status:"1",massess: "Updatecode ทำรายการสำเร็จ",usergen:get_usergen });
                                                                                                                  }
                                                                                                              });
                                                                                                          } else {
                                                                                                          }
                                                                                                      }
                                                                                                  });
                                                                                              }
                                                                                          });
                                                                                      }
                                                                                  }
                                                                              });
                                                                          }
                                                                      });
                                                                  }
                                                              }
                                                          });
                                                  } else {
                                                      //insert ได้
                                                      let insert_boxdata = "INSERT INTO `v2hbox_data` (mac_lan,mac_wifi,serial,releases,incremental,sdk_int,board,bootloader,brand,cpu_abi,cpu_abi2,device,display,fingerprint,hardware,host,v2hbox_id,manufacturer,model,product,tags,time,type,unknown,user,ipaddress,version_app,keycode1,keycode2)"
                                                          + " VALUES ('" + mac_lan_regist + "','" + mac_wifi_regist + "','" + serial_regist + "','" + release_regist + "','" + incremental_regist + "','" + sdk_int_regist + "','" + board_regist + "','" + bootloader_regist + "','" + brand_regist + "','" + cpu_abi_regist + "','" + cpu_abi2_regist + "','" + device_regist + "','" + display_regist + "','" + fingerprint_regist + "','" + hardware_regist + "','" + host_regist + "','" + id_regist + "','" + manufacturer_regist + "','" + model_regist + "','" + product_regist + "','" + tags_regist + "','" + time_regist + "','" + type_regist + "','" + unknown_regist + "','" + user_regist + "','" + ipaddress_regist + "','" + version_app_regist + "','" + keycode_regist + "','');";
                                                      sql.query(insert_boxdata, (err, result) => {
                                                          if (err) {
                                                              return res.status(500).send(err);
                                                          } else {
                                                              let select_boxdata_keycode = "SELECT v2hbox_data.id as databox_id ,keycode_v2hbox.id as keycode_id, v2hbox_data.keycode1   FROM v2hbox_data  INNER JOIN keycode_v2hbox ON  v2hbox_data.keycode1 = keycode_v2hbox.keycode1 WHERE  v2hbox_data.keycode1 = '" + keycode_regist + "'"
                                                              sql.query(select_boxdata_keycode, (err, result) => {
                                                                  if (err) {
                                                                      return res.status(500).send(err);
                                                                  } else {
                                                                      if (result.length > 1) {
                                                                          console.log("Check id boxdata and keycode < : " + result.length);
                                                                      } else if (result.length > 0) {
                                                                          let insert_usergen = "INSERT INTO `user_v2hbox`(usergen,v2hbox_data_id,keycode_v2hbox_id,expiry)"
                                                                              + "VALUES ('" + usergen + "','" + result[0].databox_id + "','" + result[0].keycode_id + "','" + expirydate + "');";
                                                                          sql.query(insert_usergen, (err, result) => {
                                                                              if (err) {
                                                                                  return res.status(500).send(err);
                                                                              } else {
                                                                                  let select_boxdata_keycode = "SELECT*FROM user_v2hbox  INNER JOIN keycode_v2hbox ON  user_v2hbox.keycode_v2hbox_id = keycode_v2hbox.id  WHERE  keycode_v2hbox.keycode1 = '" + keycode_regist + "'"
                                                                                  sql.query(select_boxdata_keycode, (err, result) => {
                                                                                      if (err) {
                                                                                          return res.status(500).send(err);
                                                                                      } else {
                                                                                          if (result.length > 0) {
                                                                                              let insert_usergen = "INSERT INTO `user_v2hbox_history`(user_v2hbox_id,keycode_id,activekey)"
                                                                                                  + "VALUES ('" + result[0].v2hbox_data_id + "','" + result[0].keycode_v2hbox_id + "',NOW());";
                                                                                              sql.query(insert_usergen, (err, result) => {
                                                                                                  if (err) {
                                                                                                      return res.status(500).send(err);
                                                                                                  } else {
                                                                                                      res.json({ status:"1",massess: "ทำรายการสำเร็จ",usergen:usergen });
                                                                                                  }
                                                                                              });
                                                                                          } else {
                                                                                          }
                                                                                      }
                                                                                  });
                                                                              }
                                                                          });
                                                                      } else {
                                                                      }
                                                                  }
                                                              });
                                                          }
                                                      });
                                                  }
                                              }
                                          });
                                      }
                                  }
                              });
                          }
                      }
                  });
                }
              }
            });
          } else {
            res.json({
              status: "0",
              massess: "Keycode ไม่ถูกต้องกรุณา ลองใหม่อีกครั่งค่ะ "
            });
          }
        }
      });
    }
  },

  checkuser: (req, res) => {
    let starus_regist = req.body.status;
    let usergen_regist = req.body.usergen;
    let maclan_regist = req.body.maclan;
    let macwifi_regist = req.body.macwifi;
    let serialnumber_regist = req.body.serialnumber;

    if (starus_regist == "1") {
      //มีuserเเล้ว
      let checkusergen =
        "SELECT * FROM `user_v2hbox` WHERE usergen = '" +
        usergen_regist +
        "' AND NOW() < expiry";
      sql.query(checkusergen, (err, result) => {
        if (err) {
          return res.status(500).send(err);
        } else {
          if (result.length > 0) {
            //มีuserเเล้ว true
            res.json({
              status: "1",
              massess: "ทำรายการสำเร็จ",
              usergen: usergen_regist
            });
          } else {
            res.json({
              status: "0",
              massess: "Keycode หมดอายุเเล้ว กรุณาติดต่อ 1264"
            });
          }
        }
      });
    } else {
      //ยังไม่มี
      let check_getuser =
        "SELECT user_v2hbox.usergen  FROM user_v2hbox INNER JOIN v2hbox_data ON v2hbox_data.id = user_v2hbox.v2hbox_data_id WHERE v2hbox_data.mac_lan = '" +
        maclan_regist +
        "' AND v2hbox_data.mac_wifi ='" +
        macwifi_regist +
        "' AND v2hbox_data.serial = '" +
        serialnumber_regist +
        "'  ";
      sql.query(check_getuser, (err, result) => {
        if (err) {
          return res.status(500).send(err);
        } else {
          if (result.length > 0) {
            //มีuserเเล้ว true
            let check_usergen =
              "SELECT * FROM `user_v2hbox` WHERE usergen = '" +
              result[0].usergen +
              "' AND NOW() < expiry";
            sql.query(check_usergen, (err, result) => {
              if (err) {
                return res.status(500).send(err);
              } else {
                if (result.length > 0) {
                  //มีuserเเล้ว true
                  res.json({
                    status: "1",
                    massess: "ทำรายการสำเร็จ",
                    usergen: result[0].usergen
                  });
                } else {
                  res.json({
                    status: "0",
                    massess: "Keycode หมดอายุเเล้ว กรุณาติดต่อ 1264 "
                  });
                }
              }
            });
          } else {
            res.json({ status: "0", massess: "ยังไม่มีการลงทะเบียน Usergen" });
          }
        }
      });
    }
  }
};
