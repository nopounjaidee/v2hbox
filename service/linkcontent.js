const express = require('express');
const router = express.Router();
const sql = require("../db");

router.post('/',(req, res) => {
    let content_id = req.body.id;
        let content_type = req.body.type;
        console.log("get id :" + content_id )
        if(content_type == "1"){
            const select_contentid2 = "SELECT contentdata.ch_name,contentdata.logo1 as logo,contentdata.link_full FROM contentdata WHERE id = '"+content_id+"' "
            sql.query(select_contentid2, (err, result) => {
                if (err) {
                  return res.status(500).send(err);
                } else {
                    if(result.length < 0){
                        console.log("lalalalal");
                    }
                    //https://load1.v2h-cdn.com/redirect/vod_cache/_definst_/path1/V2H_MTLG00128/mp4:V2H_MTLG00128_ep1_3.mp4?type=dash
                    // var link = "https://load1.v2h-cdn.com/redirect/vod_cache/_definst_/path1/"+result[0].ch_name+"/"+result[0].link_full+"?type=dash"
                    res.json({ch_name:result[0].ch_name,img:result[0].logo,link:[result[0].link_full]});
                }
            });
        }
        if(content_type == "2"){
            const select_contentid2 = "SELECT contentdata.ch_name,contentdata.logo1 as logo,contentdata.link_full FROM contentdata WHERE id = '"+content_id+"' "
            sql.query(select_contentid2, (err, result) => {
                if (err) {
                  return res.status(500).send(err);
                } else {
                    if(result.length < 0){
                        console.log("lalalalal");
                    }
                    //https://load1.v2h-cdn.com/redirect/vod_cache/_definst_/path1/V2H_MTLG00128/mp4:V2H_MTLG00128_ep1_3.mp4?type=dash
                    var link = "https://load1.v2h-cdn.com/redirect/vod_cache/_definst_/path1/"+result[0].ch_name+"/"+result[0].link_full+"?type=dash"
                    res.json({ch_name:result[0].ch_name,img:result[0].logo,link:[link]});
                }
            });
            
        }else{
            const select_contentid3 = "SELECT contentdata.ch_name,contentdata.logo1 as logo ,episode.ep_link FROM contentdata INNER JOIN episode ON contentdata.id = episode.contentdata_id WHERE contentdata.id = '"+content_id+"'"
            sql.query(select_contentid3, (err, result) => {
                if (err) {
                  return res.status(500).send(err);
                } else {
                    if(result.length < 0 ){
                        console.log("lalalalal");
                    }
                    var eps = [];
                    for(var i = 0;i < result.length;i++){
                        
                        var eplink = "https://load1.v2h-cdn.com/redirect/vod_cache/_definst_/path1/"+result[0].ch_name+"/"+result[i].ep_link+"?type=dash"
                         eps.push(eplink);
                        
                    }
                    // console.log(eps);
                    res.json({ch_name:result[0].ch_name,img:result[0].logo,link:eps});
                }
            });
        }


});
router.get('/:chname/:type',(req,res)=>{
var chname = req.params.chname
var type = req.params.type

if(type == "movie"){
    const select_contentid2 = "SELECT contentdata.ch_name,contentdata.logo1 as logo,contentdata.link_full FROM contentdata WHERE ch_name = '"+chname+"' "
    sql.query(select_contentid2, (err, result) => {
        if (err) {    
          return res.status(500).send(err);
        } else {
            if(result.length > 0){
                
                // console.log(chname + type );
            //https://load1.v2h-cdn.com/redirect/vod_cache/_definst_/path1/V2H_MTLG00128/mp4:V2H_MTLG00128_ep1_3.mp4?type=dash
            var link = "https://load1.v2h-cdn.com/redirect/vod_cache/_definst_/path1/"+result[0].ch_name+"/"+result[0].link_full+"?type=dash"
            res.json({ch_name:result[0].ch_name,img:result[0].logo,link:[link]});
            }
            res.json({message:"ไม่มีข้อมูล"});
        }
    });

    
    
}else{
    const select_contentid3 = "SELECT contentdata.ch_name,contentdata.logo1 as logo ,episode.ep_link FROM contentdata INNER JOIN episode ON contentdata.id = episode.contentdata_id WHERE contentdata.ch_name = '"+chname+"'"
    sql.query(select_contentid3, (err, result) => {
        if (err) {
          return res.status(500).send(err);
        } else {
            if(result.length > 0 ){
                
                var eps = [];
            for(var i = 0;i < result.length;i++){
                
                var eplink = "https://load1.v2h-cdn.com/redirect/vod_cache/_definst_/path1/"+result[0].ch_name+"/"+result[i].ep_link+"?type=dash"
                 eps.push(eplink);
                
            }
            // console.log(eps);
            res.json({ch_name:result[0].ch_name,img:result[0].logo,link:eps});
            }
            res.json({message:"ไม่มีข้อมูล"});
            
        }
    });
}
});
module.exports = router;
