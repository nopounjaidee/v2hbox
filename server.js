var log4js = require('log4js');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
require('./db')
var log = log4js.getLogger("app");
var app = express();
app.use(log4js.connectLogger(log4js.getLogger("http"), { level: 'auto' }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(function(req,res,next){
    res.setHeader('Access-Control-Allow-Origin','*');
    //res.setHeader('Access-Control-Allow-Origin','http://localhost:4200');
    res.setHeader('Access-Control-Allow-Methods','GET, POST, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers','content-type, x-access-token');
    res.setHeader('Access-Control-Allow-Credentials','*');
    next();
});


//const {getversion} = require('./service/vsapp');
const {ma_starus,checkversion,gen_keycode} = require('./service/ma_service');
const {register,checkuser} = require('./service/regist_v2hbox');
const {menu_mainpage,get_content_data,get_content_data_subtab,get_content_liverecommen,get_content_recommen,content_data,content_name,continue_watching} = require('./service/ma_content');
const {get_content_movierecommen,get_content_serierecommen,get_content_varityrecommen} = require('./service/demo_api');


app.get('/maintenance', ma_starus);//** */
app.post('/addkeycode', gen_keycode);//** */
app.post('/version', checkversion);
app.post('/register', register);
app.post('/checkuser', checkuser);
//Connect
app.post('/menu_mainpage', menu_mainpage);
app.post('/liverecommen', get_content_liverecommen);
app.post('/recommen', get_content_recommen);
app.post('/content_data', get_content_data);
app.post('/content_datasubtab', get_content_data_subtab);
app.post('/content', content_data);
app.post('/continue_watching', continue_watching);
//+ link
app.post('/contentname', content_name);
const linkcontent = require('./service/linkcontent');
app.use('/content_name',linkcontent);

//API Demo
app.post('/movierecommen',get_content_movierecommen);
app.post('/serierecommen',get_content_serierecommen);
app.post('/varityrecommen',get_content_varityrecommen);


/// catch 404 and forward to error handler
app.use(function(req, res, next) {
    console.log("Link Part ไม่ถูกต้อง !! ");
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});
// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    log.error("Something went wrongttt:", err);
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;
